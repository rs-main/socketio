// Setup basic express server
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
const fetch = require("node-fetch");
var port = process.env.PORT || 3000;
var serverName = process.env.NAME || 'Unknown';
const redis = require("redis");
const password = process.env.password || 'password123';
const redisPort = 6380;
const database  = process.env.DATABASE || 0;
const redisServerIP = "18.184.172.239";

const REDIS_SERVER = `redis://${redisServerIP}:${redisPort}?database=${database}&password=${password}`;
const WEB_SOCKET_PORT = 3001;

var redisClient = redis.createClient(REDIS_SERVER);

redisClient.subscribe('app:notifications');

redisClient.on("message",function (channel, message){
  console.log(`redis client message ${message} from channel ${channel}`);
});


server.listen(port, function () {
  console.log('Server listening at port %d', port);
  // console.log('Hello, I\'m %s, how can I help?', serverName);
});

// Routing
app.use(express.static(__dirname + '/public'));

// Chatroom

var numUsers = 0;

var counts = 0;

io.on('connection', function (socket) {

  redisClient.on("message",function (channel, message){
    console.log(`redis client message ${message}`);
    if (counts > 0){
      counts = 0;
      return;
    }
    socket.broadcast.emit('new message', {
      message: JSON.parse(message)
    });
    counts++;
  });

  // socket.emit('my-name-is', serverName);

  var addedUser = false;

  // when the client emits 'new message', this listens and executes
  socket.on('messageSent', function (data) {
    // we tell the client to execute 'new message'
    socket.broadcast.emit('messageSent', {
      message: "socket communication works"
    });
  });

  // when the client emits 'add user', this listens and executes
  socket.on('add user', function (username) {
    if (addedUser) return;

    // we store the username in the socket session for this client
    socket.username = username;
    ++numUsers;
    addedUser = true;
    socket.emit('login', {
      numUsers: numUsers
    });
    // echo globally (all clients) that a person has connected
    socket.broadcast.emit('user joined', {
      username: socket.username,
      numUsers: numUsers
    });
  });

  // when the client emits 'typing', we broadcast it to others
  socket.on('typing', function () {
    socket.broadcast.emit('typing', {
      username: socket.username
    });
  });

  // when the client emits 'stop typing', we broadcast it to others
  socket.on('stop typing', function () {
    socket.broadcast.emit('stop typing', {
      username: socket.username
    });
  });

  // when the user disconnects.. perform this
  socket.on('disconnect', function () {
    socket.removeAllListeners('new message');
    socket.removeAllListeners('disconnect');
    io.removeAllListeners('connection');
    // if (addedUser) {
    //   --numUsers;
    //
    //   // echo globally that this client has left
    //   socket.broadcast.emit('user left', {
    //     username: socket.username,
    //     numUsers: numUsers
    //   });
    // }
  });
});

